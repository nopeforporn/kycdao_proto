const gulp = require("gulp"),
  sass = require('gulp-sass')(require('sass')),
  concat = require("gulp-concat"),
  livereload = require('gulp-livereload'),
  lr = require('tiny-lr'),
  server = lr();

function styles() {
  return gulp.src("src/assets/styles/main.scss")
    .pipe(sass())
    .pipe(gulp.dest("www/assets/styles"))
    .pipe(livereload(server));
}

function scripts() {
  return gulp.src("src/assets/js/**/**.js")
    .pipe(concat("main.js"))
    .pipe(gulp.dest("www/assets/js"))
    .pipe(livereload(server));
}


gulp.task('sass', function() {
  return gulp.src("src/assets/styles/main.scss")
      .pipe(sourcemaps.init())
      .pipe(sass())
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest('assets/css'))
      .pipe(bs.reload({
        stream: true
      }));
});


function images() {
  return gulp.src("src/assets/images/**/*")
    .pipe(gulp.dest("www/assets/images"))
    .pipe(livereload(server));
}

gulp.task("watch", function () {

  gulp.watch("src/assets/styles/**/**.scss", styles);
  gulp.watch("src/assets/js/**/**.js", scripts);
  gulp.watch("src/assets/images/**/*", images);

});